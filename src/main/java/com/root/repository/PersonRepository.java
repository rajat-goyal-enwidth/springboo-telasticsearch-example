package com.root.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import com.root.document.Person;

public interface PersonRepository extends ElasticsearchRepository<Person, String> {
	
	
}